// const { exec } = require('child_process');
const { writeFileSync, createWriteStream, existsSync, mkdir } = require("fs");
const { got } = require("got-cjs");
const { JSDOM } = require("jsdom");
const { slugify, getFileName, info, error, warn, success } = require("./utils");
const { urls } = require("./urls.js");
const { streamFile } = require("./streamFile.js");

// const urls = ["89331v1", "88955v1", "89082v1"];

// new ost
// "https://elifesciences.org/reviewed-preprints/86990/pdf",
// const host = "https://staging--epp.elifesciences.org/reviewed-preprints";
//
const host = "";

scrap(urls);
const errorFiles = [];
const urlFailure = [];
async function scrap(urls) {
  console.log(urls);
  let batchlist = [];

  await urls?.forEach(async (url, index) => {
    got(`https://elifesciences.org/reviewed-preprints/${url}/pdf`)
      .then(async (response) => {
        // console.log(response)
        try {
          const id = await createSinglePage(response.body, index);
          batchlist.push(id);
          success(`${id} - initial process completed`);
          writeFileSync(
            "togenerate.js",
            `const urls = [${batchlist}]
module.exports = { urls }`,
          );
        } catch (err) {
          urlFailure.push(url);
          error(`${url} an error occurred`, err);
        }
      })
      .catch((err) => {
        error(`${url} error in url access`, err);
      });
  });

  if (urlFailure.length) error("Failed URLS:", urlFailure);
}

async function createSinglePage(content, index) {
  let doc = new JSDOM(content);
  let document = doc.window.document;

  const titleElem = document.querySelector("h1");

  // console.log(titleElem);
  const title = titleElem?.innerHTML;
  // for all links check if there is a doi in there.
  // stop after the first doi

  const doi = document
    .querySelector(".descriptors a[href*=doi]")
    .innerHTML.split("-->")[1];

  // generatedDOIs.push(doi);
  document.querySelector(".descriptors a[href*=doi]").innerHTML =
    `https://doi.org/${doi}`;

  // console.log(document.querySelector('.descriptors a[href*=doi]').innerHTML)

  // we don’t need the stylesheet now.
  // if (index === 0) {
  //   const styleSrc = document
  //     .querySelector('link[rel="stylesheet"]')
  //     .getAttribute("href");
  //   got(`${host}${styleSrc}`)
  //     .then((response) => {
  //       writeFileSync(`./static/css/default.css`, response.body);
  //     })
  //     .catch((err) => error(`error in ${id}`, err));
  // }

  document.querySelectorAll("script")?.forEach((script) => {
    script.remove();
  });
  document.querySelectorAll("head")?.forEach((head) => {
    head.remove();
  });
  document.querySelectorAll("style")?.forEach((style) => {
    style.remove();
  });
  document.querySelectorAll("link")?.forEach((link) => {
    link.remove();
  });

  // addReviews(peerReviews)

  // if (theReviews) {
  //   document
  //     .querySelector('#author-list')
  //     .insertAdjacentHTML('afterend', theReviews)
  // }

  // get the first word of the title to get an ID
  const id = getFileName(slugify(doi));

  updatedImgURLs(document.documentElement, id);

  writeFileSync(
    `./src/content/preprints/${id}.html`,
    `---\n
date: "${new Date().toISOString()}"
title: "${title}" \n
doi: "${doi}" \n
pdf: false\n
id: ${id}\n
peerReview: ""
---\n\n
${document.documentElement?.innerHTML}`,
  );

  if (!existsSync(`./static/css/${id}.css`))
    writeFileSync(`./static/css/${id}.css`, "");

  return id;
  // try {
  //   const resp = await generatePDF(doi)
  //   return resp;
  // } catch (error) {
  //   throw error;
  // }
  // change img urls
  // recreate the article from the part
  // remove scripts
}

async function updatedImgURLs(content, id) {
  // test if the folder exists for that id exists
  const dir = `./static/images/${id}`;

  try {
    if (existsSync(dir)) {
      warn(`dir ${dir} exist`);
    } else {
      mkdir(`static/images/${id}`, (err) => {
        if (err) {
          error(`error in ${id} - folder creation`, err);
          return;
        }
        info(`${id} folder created ok - note: images might still be streaming`);
      });
    }

    // use axios to get the images and render them.

    const { fetchAll, imgIndex } = refetchImages(id);

    if (!fetchAll && !imgIndex.length) return;
    const promises = [];
    content?.querySelectorAll("img")?.forEach((img, index) => {
      try {
        let imageType;
        if (imgIndex.length && !imgIndex.includes(index)) return;
        // remove styles on image
        img.style = "";

        if (!img.src) {
          warn(` ${id} - skipping image ${index} - no src`);
          return;
        }

        if (img.src?.[0] === "/") {
          warn(
            ` ${id} - image ${index} - added host - since it starts with /, src: ${img.src}`,
          );
          img.src = `https://prod--epp.elifesciences.org/${img.src}`;
        }
        // download all images

        // check if it’s a base64 svg. then recreate a svg file with a fake name
        if (img.src.includes("data:image/svg+xml")) {
          let svgData = img.src.split("svg+xml,")[1];
          console.log(id, `in svg xml`, index);
          return console.log("data64 → " + svgData);
        } else if (img.src.includes("data:image/gif")) {
          let content = decodeBase64Image(img.src, id);
          console.log(id, `in b64`, index);
          createWriteStream(
            `./static/images/${id}/gif-${index}.gif`,
            content,
            function(err) {
              if (err) {
                error(`err in ${id} - data:gif`, err);
              }
            },
          );
          return;
        } else if (img.src.includes(".gif")) {
          // create a src image
          // let src = img.src.split("%2F")[2]?.split(".gif")[0];
          imageType = "gif";
        } else if (img.src.includes(".svg")) {
          // let src = img.src.split("%2F")[2]?.split(".tif")[0];
          imageType = "svg";
        } else {
          // create a src image
          // let src = img.src.split("%2F")[2]?.split(".tif")[0];
          imageType = "jpg";
        }

        if (imageType) {
          // we get also the height and width of any image from dataoriginal height
          let imgURL = `${host}${img.src}`;

          const baseImgPath = `images/${id}/img-${index}.${imageType}`;
          const filePath = `./static/${baseImgPath}`;
          img.insertAdjacentHTML(
            "beforebegin",
            `<img src="{{ '../' | url }}${baseImgPath}" 
            ${img.id ? `id="${img.id}"` : ""}
            ${img.dataset.originalHeight ? `height="${img.dataset.originalHeight}"` : ""}
            ${img.dataset.originalWidth ? `width="${img.dataset.originalWidth}"` : ""} >`
          );
          img.remove();

          promises.push(
            streamFile({
              url: imgURL,
              filePath,
              successCallback: () => success(`File downloaded to ${filePath}`),
              failureCallback: ({ error: err }) => {
                errorFiles.push({
                  filePath,
                  err,
                });
                error(
                  `error in ${id} - ${index} image, src: ${img?.src}`,
                  err && typeof err === "object"
                    ? JSON.stringify(err, null, 2)
                    : null,
                );
              },
            }),
          );
        }
      } catch (err) {
        console.log(`there was a error downloading the image from ${id}`);
        // error(`error in ${id} - ${index} image, src: ${img?.src}`, err);
      }
    });

    try {
      /*
       * fetching images of 1 file parallelly
       * error file are updated after every file
       * since errorFiles is a global variable, it won't erase previous data
       */
      await Promise.allSettled(promises);
      const errStr = JSON.stringify(errorFiles ?? [], null, 2);
      writeFileSync(
        "errorFiles.js",
        `const errorFiles = ${errStr}; module.exports = { errorFiles };`,
      );
    } catch (error) {
      console.log(`there was a error downloading the image from ${id}`);
    }

    info(`image streaming process complete for ${id}`);
  } catch (err) {
    error(`err in ${id}`, err);
    return;
  }
}

/**
 * Decode base64 image
 * https://gist.github.com/barbietunnie/5fa07012925ee0fe53a0
 *.e.g. data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAAPAQMAAABeJUoFAAAABlBMVEX///8AAABVwtN+AAAAW0lEQVQImY2OMQrAMAwDjemgJ3jI0CFDntDBGKN3hby9bWi2UqrtkJAk8k/m5g4vGBCprKRxtzQR3mrMlm2CKpjIK0ZnKYiOjuUooS9ALpjV2AjiGY3Dw+Pj2gmnNxItbJdtpAAAAABJRU5ErkJggg==
 */

function decodeBase64Image(dataString, id) {
  try {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
      response = {};

    if (matches.length !== 3) {
      return new Error("Invalid input string");
    }

    response.type = matches[1];
    response.data = Buffer.from(matches[2], "base64");

    return response;
  } catch (err) {
    error(`err in ${id}`, err);
  }
}

function refetchImages(id) {
  console.log("id", id);
  /* NOTE:
   * solution not complete
   * Stage/commit previous HTML before re-running scrapper
   * Remove new HTML after scrap
   * */
  const images = {
    // 88189.1: [3, 15, 16, 17, 19, 20, 21, 22, 23, 24, 28, 29, 30, 32],
    // 86721.1: [2, 3, 4, 5, 8, 9],
  };
  if (!Object.keys(images).length) return { fetchAll: true, imgIndex: [] };
  if (images[id] && images[id]?.length)
    return { fetchAll: false, imgIndex: images[id] };
  return { fetchAll: false, imgIndex: [] };
}
