const { got } = require("got-cjs");
const { createWriteStream } = require("fs");
const { pipeline: streamPipleline } = require("stream");
const { promisify } = require("util");
const pipeline = promisify(streamPipleline);

// const url = "https://media0.giphy.com/media/4SS0kfzRqfBf2/giphy.gif";
// const filePath = "image.gif";

const streamFile = async ({
  url = "",
  filePath = "",
  maxRetry = 0,
  logProgress = false,
  retryDelay = 1000,
  successCallback = function () {
    return;
  },
  failureCallback = function ({ error }) {
    return;
  },
  retryCallback = function ({ retryNumber, error }) {
    return;
  },
}) => {
  if (!filePath || !url)
    throw new Error("Please enter valid file path and URL");
  const downloadStream = got.stream(url);
  const fileWriterStream = createWriteStream(filePath);

  if (logProgress)
    downloadStream.on("downloadProgress", ({ transferred, total, percent }) => {
      const percentage = Math.round(percent * 100);

      console.log(`progress: ${transferred}/${total} (${percentage}%)`);
    });

  const addDataToFile = async (retryNumber = 0) => {
    try {
      await pipeline(downloadStream, fileWriterStream);
      if (successCallback) successCallback();
    } catch (error) {
      if (retryNumber < maxRetry) {
        setTimeout(() => retryCallback({ retryNumber, error }), retryDelay);

        addDataToFile(retryNumber + 1);
      } else {
        if (failureCallback) failureCallback({ error });
        // error(`File Download ${filePath} failed, ${error.message}`);
      }
    }
  };
  return await addDataToFile();
};
module.exports = { streamFile };
