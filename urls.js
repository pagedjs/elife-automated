const w45 = [
  "92093v1",
  "92063v1",
  "88236v2",
  "91953v1",
  "91495v1",
  "89004v2",
  "89891v2",
  "91711v1",
  "91826v1",
  "92731v1",
  "90849v1",
  "88387v1",
  "89331v3",
  "89354v1",
  "90027v2",
  "90440v2",
  "90306v1",
  "90724v1",
  "90875v1",
  "91260v1",
  "89423v1",
  "91949v1",
  "92938v1",
  "92443v1",
  "92342v1",
  "91102v1",
  "90799v1",
  "91650v1",
  "88321v2",
  "92175v1",
  "88919v2",
  "90510v2",
  "87022v2",
  "91928v1",
  "90755v2",
  "90486v2",
  "90505v1",
  "90636v2",
];

const w46 = [
  "92615v1",
  "92004v1",
  "92462v1",
  "91422v1",
  "91392v1",
  "91046v1",
  "90316v1",
  "89656v2",
  "89232v2",
  "88658v2",
  "88579v2",
  "88491v2",
  "88117v3",
  "91318v1",
  "91007v2",
  "91554v1",
  "88604v2",
  "87969v2",
  "91647v1",
  "92889v1",
  "91765v1",
  "88745v2",
  "88183v2",
  "91387v1",
  "91316v1",
  "92173v1",
  "91135v1",
  "90780v1",
  "90184v2",
  "91582v1",
  "91033v1",
  "84141v2",
  "86749v2",
  "91168v1",
  "89493v2",
  "89754v1",
  "87833v2",
  "91705v1",
  "91498v1",
  "87518v2",
  "87611v2",
  "88087v2",
  "88637v2",
  "88659v2",
  "89467v2",
  "91011v1",
  "91861v2",
  "91976v1",
  "92201v1",
  "90214v2",
  "91605v1",
  "91933v1",
  "91825v1",
  "90497v1",
  "89795v2",
  "89682v2",
  "89317v3",
  "91609v1",
  "90989v1",
  "90173v1",
  "92746v1",
  "92829v1",
  "92958v1",
  "92979v1",
  "88008v3",
  "89373v2",
  "89635v2",
  "90132v1",
];

const htmlRerender = ["87450v1", "90511v1", "90333v2"];
const chunkSize = 7;
const arrayNumber = 0;

function chunkArray(array, chunkSize, arrayNumber) {
  if (chunkSize <= 0) {
    throw new Error("Chunk size should be greater than 0");
  }

  const chunkedArray = [];
  for (let i = 0; i < array.length; i += chunkSize) {
    const chunk = array.slice(i, i + chunkSize);
    chunkedArray.push(chunk);
  }
  return chunkedArray[arrayNumber];
}

const urls = chunkArray(htmlRerender, chunkSize, arrayNumber);

module.exports = { urls };
