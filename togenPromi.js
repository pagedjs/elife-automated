const { promisify } = require("util");
const { exec } = require("child_process");
const fs = require("fs");
const { slugify, getFileName, error } = require("./utils");

// const { urls } = require("./togenerate.js");
const { urls } = require("./alltogenerate.js");

const executeCommand = promisify(exec);

const generatePDF = async (filename) => {
  try {
    console.log(`generating ${filename}`)
    const baseScript = `pagedjs-cli ./public/10.7554elife.${filename}/index.html`;
    const additionnalScripts = `--additional-script ./public/js/csstree.js --additional-script ./public/js/elife-custom-scripts.js --additional-script ./public/js/pagedjs-fill-page.js --additional-script ./public/js/pagedjs-plugin-baseline.js`;
    const generatePDFCommand = `${baseScript}  ${additionnalScripts} -o ./static/outputs/${filename}.pdf`;
    const debugPDFCommand = `${generatePDFCommand} --debug`;

    await executeCommand(generatePDFCommand);
    
    console.log(`------------${filename}.pdf created successfully----------`);
  } catch (err) {
    console.error(`--------- error in ${filename}---------`, err);
    
    // Log the error to an MD file
    const errorLog = `# Error in ${filename}\n\n\`\`\`\n${err}\n\`\`\`\n\n`;
    fs.appendFileSync("error_log.md", errorLog);
  }
};

const generatePDFsInBatches = async (batchSize) => {
  for (let i = 0; i < urls.length; i += batchSize) {
    const batch = urls.slice(i, i + batchSize);
    await Promise.all(batch.map(generatePDF));
  }
};

const batchSize = 20;
generatePDFsInBatches(batchSize);
