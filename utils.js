const slugify = (string) =>
  string.split(" ").join("").replace("/", "").toLowerCase();

const getFileName = (slugifiedDOI) =>
  `${slugifiedDOI.split(".")[2]}.${slugifiedDOI.split(".")[3] ?? 1}`;

const CONSOLE_COLORS = {
  blue: "\x1b[34m",
  green: "\x1b[32m",
  red: "\x1b[31m",
  white: "\x1b[37m",
  yellow: "\x1b[33m",
  end: "\x1b[0m",
};

const info = (text) =>
  console.info(CONSOLE_COLORS.blue, `INFO: `, CONSOLE_COLORS.end, text);
const warn = (text) =>
  console.warn(CONSOLE_COLORS.yellow, `WARN: `, CONSOLE_COLORS.end, text);
const error = (text, errorObj = null) =>
  console.error(
    CONSOLE_COLORS.error,
    `ERROR: `,
    CONSOLE_COLORS.end,
    text,
    errorObj ?? ""
  );
const success = (text) =>
  console.info(CONSOLE_COLORS.green, `SUCCESS: `, CONSOLE_COLORS.end, text);
module.exports = {slugify, getFileName, info, error, warn, success};