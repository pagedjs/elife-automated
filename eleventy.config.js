const yaml = require("js-yaml");

module.exports = function (eleventyConfig) {
  // eleventyConfig.addFilter("searchSingle", searchFilterSingle);
  eleventyConfig.addCollection("preprints", (collection) => {
    return [
      ...collection.getFilteredByGlob("./src/content/preprints/**.html"),
    ].sort((a, b) => new Date(b?.date) - new Date(a?.date));
  });

  eleventyConfig.addCollection("customhtml", (collection) => {
    return [
      ...collection.getFilteredByGlob("./src/content/customhtml/**.html"),
    ];
  });

  eleventyConfig.addFilter("not", function (array, key, value) {
    if (!array) return;
    return array.filter((el) => {
      return el[key] != value;
    });
  });
  eleventyConfig.addFilter("with", function (array, key, value) {
    if (!array) return;
    return array.filter((el) => {
      return el[key] == value;
    });
  });
  eleventyConfig.addFilter("notcontains", function (array, key, value) {
    if (!array) return;
    return array.filter((el) => {
      return !el[key].includes(value);
    });
  });
  eleventyConfig.addFilter("contains", function (array, key, value) {
    if (!array) return;
    return array.filter((el) => {
      return el[key].includes(value);
    });
  });

  // yaml as data

  eleventyConfig.addDataExtension("yaml, yml", (contents) =>
    yaml.load(contents)
  );

  eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });

  eleventyConfig.addFilter(
    "getDOI",
    (rawString) => rawString.slice(13, -1) ?? rawString
  );
  eleventyConfig.addFilter("sortById", (array = []) =>
    array.sort((a, b) => a?.id - b?.id ?? a?.data?.id - b?.data?.id)
  );

  // plugin TOC

  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};
