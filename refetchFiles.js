const { errorFiles } = require("./errorFiles");
const { streamFile } = require("./streamFile");
const { success, error } = require("./utils");

errorFiles?.forEach((file) => {
  const url = file.err.options.url;

  const filePath = file.filePath;

  try {
    streamFile({
      url,
      filePath,
      logProgress: true,
      successCallback: () => success(`File downloaded to ${filePath}`),
      failureCallback: ({ error: err }) =>
        error(
          `RETRY ERROR in ${filePath} ${url}`,
          err && typeof err === "object" ? JSON.stringify(err, null, 2) : null
        ),
    });
  } catch (err) {
    error(
      `RETRY ERROR Caught in ${filePath} ${url}`,
      err && typeof err === "object" ? JSON.stringify(err, null, 2) : null
    );
  }
});


/** Sample Errors */
// [
//   {
//     "filePath": "./static/images/90724.1/img-13.jpg",
//     "err": {
//       "code": "ENOTFOUND",
//       "timings": {
//         "start": 1700723206329,
//         "socket": 1700723206331,
//         "lookup": 1700723206347,
//         "error": 1700723206347,
//         "phases": {
//           "wait": 2,
//           "dns": 16,
//           "total": 18
//         }
//       },
//       "name": "RequestError",
//       "options": {
//         "agent": {},
//         "decompress": true,
//         "timeout": {},
//         "prefixUrl": "",
//         "ignoreInvalidCookies": false,
//         "context": {},
//         "hooks": {
//           "init": [],
//           "beforeRequest": [],
//           "beforeError": [],
//           "beforeRedirect": [],
//           "beforeRetry": [],
//           "afterResponse": []
//         },
//         "followRedirect": true,
//         "maxRedirects": 10,
//         "throwHttpErrors": true,
//         "username": "",
//         "password": "",
//         "http2": false,
//         "allowGetBody": false,
//         "headers": {
//           "user-agent": "got (https://github.com/sindresorhus/got)",
//           "accept-encoding": "gzip, deflate, br"
//         },
//         "methodRewriting": false,
//         "retry": {
//           "limit": 2,
//           "methods": [
//             "GET",
//             "PUT",
//             "HEAD",
//             "DELETE",
//             "OPTIONS",
//             "TRACE"
//           ],
//           "statusCodes": [
//             408,
//             413,
//             429,
//             500,
//             502,
//             503,
//             504,
//             521,
//             522,
//             524
//           ],
//           "errorCodes": [
//             "ETIMEDOUT",
//             "ECONNRESET",
//             "EADDRINUSE",
//             "ECONNREFUSED",
//             "EPIPE",
//             "ENOTFOUND",
//             "ENETUNREACH",
//             "EAI_AGAIN"
//           ],
//           "backoffLimit": null,
//           "noise": 100
//         },
//         "method": "GET",
//         "cacheOptions": {},
//         "https": {},
//         "resolveBodyOnly": false,
//         "isStream": true,
//         "responseType": "text",
//         "url": "https://prod--epp.elifesciences.org/iiif/2/90724%2Fv1%2F549410v1_fig4s2.tif/full/max/0/default.jpg",
//         "pagination": {
//           "countLimit": null,
//           "backoff": 0,
//           "requestLimit": 10000,
//           "stackAllItems": false
//         },
//         "setHost": true,
//         "enableUnixSockets": true
//       }
//     }
//   }
// ]