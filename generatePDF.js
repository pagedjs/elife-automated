const { exec } = require("child_process");
const { error } = require("./utils");

const { urls } = require("./togenerate.js");



const generatePDF = async (filename) => {
  // if (!doi || typeof doi !== "string")
  //   error(
  //     `invalid DOI ${doi === null || doi === undefined ? doi : JSON.stringify(doi, null, 2)
  //     }`
  //   );

   try {
    const baseScript = `pagedjs-cli ./public/10.7554elife.${filename}/index.html`;
    const additionnalScripts = `--additional-script ./public/js/csstree.js --additional-script ./public/js/elife-custom-scripts.js --additional-script ./public/js/pagedjs-fill-page.js --additional-script ./public/js/pagedjs-plugin-baseline.js`;
    const generatePDF = `${baseScript}  ${additionnalScripts} -o ./static/outputs/${filename}.pdf`;
    // const debugPDF = `${generatePDF} --debug`;
    exec(`${generatePDF}`, (error) => {
      if (error) {
        console.error(`--------- error in ${filename}---------`, error);
      }
      console.log(`------------${filename}.pdf created successfully----------`);
    });
  } catch (err) {
    error(`An error occurred - ${filename}`, err);
  }
};

const generatePDFsInBatches = async (batchSize, urls) => {
  for (let i = 0; i < urls.length; i += batchSize) {
    const batch = urls.slice(i, i + batchSize);
    await Promise.all(batch.map(generatePDF));
  }
};

const batchSize = 5;


generatePDFsInBatches(batchSize, urls)

// urls?.forEach(generatePDF);


