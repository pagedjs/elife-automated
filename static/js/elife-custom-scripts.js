// Set the handler
class elifeBuild extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    //fix elife internal links

    content.querySelectorAll(".review-timeline a").forEach((a) => {
      if (
        a.textContent.trim() === "Not revised" ||
        a.textContent.trim() === "Revised by authors"
      ) {
        a.removeAttribute("href");
      } else if (a.href.startsWith("file://")) {
        const newurl = a.href.split("/")[a.href.split("/").length - 1];
        a.href = "https://elifesciences.org/reviewed-preprints/" + newurl;
      } else if (a.href.startsWith("/")) {
        const newurl = a.href.split("/")[a.href.split("/").length - 1];
        a.href = "https://elifesciences.org/reviewed-preprints/" + newurl;
      }
    });

    [
      "#supplementary-material a",
      "#additional-files a",
      "#supplementary-figures a",
      "#supplementary-materials a",
    ].forEach((selector) => {
      content.querySelectorAll(selector).forEach((a) => {
        const oldHref = a.getAttribute("href");
        const newHref = `https://prod--epp.elifesciences.org/api/files/${oldHref}`;
        a.setAttribute("href", newHref);
      });
    });

    // recreate the html we want.
    remixContent(content);

    // the pictu"e element needs to be remove
    // removePictures(content);
    guessSupp(content);

    // images
    markImages(content);

    //add width and height to image map the sizing
    addCorrespondingAuthor(content);

    // truncate the long references (why would you do that?)
    truncateRefernceAuthors(content);

    //get the copyright from the CC icon when the icon is on top
    fixCClink(content);

    //check the igures and mark them
    // check will page will be set as full page
    tagFigures(content);

    // clean and reorder figures
    reorderfigures(content);

    // mrge blockquote together
    mergeBlockquotes(content);

    // recreate the html we want.
    cleanLinks(content);

    // fig figure names
    fixFigureLabel(content);

    // to divide figure in block
    dispatchFigure(content);

    // to divide figure in block
    fixAbstract(content);

    // remove any noscript eelement
    removeNoScript(content);

    // if the paragraph only contains a number, add a class to it
    // used to fix very specific blocks
    hidenumber(content);

    // remove any script eelement
    removescript(content);

    // transform space to nbsp
    spaceToNbsp(content);

    // trim the article flag
    trimArticleFlags(content);

    // fix the head and generate the running head
    createElForRunningHead(content); // ←

    // add logo with link to the header
    addLogoWithLink(content);

    // wrap the references in a section with a class refrence
    wrapReferences(content);

    // makeTitle from p with strong
    pStrongToTitles(content);

    //tag figure
    tagImgInFigures(content);

    // this was here to manage title in the sub section. maybe not needed anymore
    let titlesh1 = content.querySelectorAll("h1, h2");

    // removing this because we now have the section for the supplementary figures / files, etc.

    // fix the never ending titles
    // let titles1 = content.querySelectorAll("h1, h2, h3, h4");

    // titles1.forEach((title, index) => {
    //   if (!title.nextElementSibling) {
    //     // console.log("title", title)
    //     title.insertAdjacentHTML(
    //       "afterend",
    //       `<p class="widowfix" style="color: transparent">nothing here</p> `,
    //     );
    //   } else if (title.nextElementSibling?.tagName == title.tagName) {
    //     title.nextElementSibling.insertAdjacentHTML(
    //       "afterend",
    //       `<p class="widowfix" style="color: transparent">nothing here</p>`,
    //     );
    //   } else if (title.nextElementSibling.classList.contains("movedfig")) {
    //     console.log(title);

    // title.nextElementSibling.insertAdjacentHTML(
    //   "beforebegin",
    //   `<p class="widowfix fig" style="color: transparent">nothing here</p>`,
    // );

    //   title.nextElementSibling.insertAdjacentHTML(
    //     "afterbegin",
    //     `<p class="widowfix widowfix-fig" style="color: transparent">nothing here</p>`,
    //   );
    // }
    // });

    // add id to anything to fix things
    addIDtoEachElement(content);

    // add small bit that are clickable when needed.
    makeAllLinksClickable(content);

    // attempt to fix when there is text between figures   title between figures
    let supfigures = content
      .querySelector(
        "#supplemental-figures , #figure-supplements, [id*=supplem]"
      )
      ?.querySelectorAll("figure");
    supfigures?.forEach((sup) => {
      if (
        !sup.nextElementSibling ||
        (sup.nextElementSibling && sup.nextElementSibling.tagName != "FIGURE")
      ) {
        sup.classList.add("last-figure");
        sup.insertAdjacentHTML("afterend", `<div class="pagebreak"></div>`);
      }
    });
  }

  afterRendered(pages) {
    recreateRunningHead();
    //only for elife, check the runningheadre
    // add the running head
    let runninghead = document.querySelector(".runninghead");
    document
      .querySelectorAll(".pagedjs_pagedjs-filler_page")
      .forEach((page) => {
        // this is for elife only, fix the runningHead
        if (runninghead) {
          page
            .querySelector(".pagedjs_pagebox")
            .insertAdjacentElement("afterbegin", runninghead.cloneNode(true));
        }
      });
  }
}

Paged.registerHandlers(elifeBuild);

//is last element?
//
function isLastElement(element) {
  // Get the parent element
  var parentElement = element.parentNode;

  // Check if the element is the last child of its parent
  if (parentElement && parentElement.lastChild === element) {
    return true;
  }

  // If there is no parent element, return false
  return false;
}

///----//----//----//----//----//----//----//----//----/----//

function wrapReferences(content) {
  if (content.querySelector("h1#references")) {
    let references = content.querySelector("h1#references");
    references.closest("section").id = "references-wrapper";
    content.querySelector("h1#references").id = "references-title";
  }
}

function fixFigureLabel(content) {
  // remove `:` when the label end with `:`
  // console.log(content.querySelectorAll(".figure__label"))
  content.querySelectorAll(".label").forEach((el) => {
    if (el.innerHTML.trim().endsWith(":")) {
      el.innerHTML = el.innerHTML.trim().slice(0, -1);
    }
    // console.log(el);
    // el.innerHTML = el.innerHTML.replace("Fig.", "Figure ");
  });
}

function trimArticleFlags(content) {
  content.querySelectorAll(".article-flag-list li")?.forEach((item) => {
    item.textContent = item.textContent.trim();
  });
}

function removeNoScript(content) {
  content.querySelectorAll("noscript").forEach((thing) => {
    thing.remove();
  });
}

function cleanLinks(content) {
  // add wbr2links
  content.querySelectorAll("a").forEach((link) => {
    //backup strat
    if (link.classList.contains("donttouch")) return;

    if (!link.href || link.href?.length == 0) {
      return console.log("link missig:", link);
    }

    if (link.querySelector("img")) {
      // link.querySelector("img").classList.add("img-in-link");
      if (isLinkContainingOnlyImage(link)) {
        if (isElementTheOnlyChild(link)) {
          link.parentElement.insertAdjacentElement(
            "afterend",
            link.querySelector("img")
          );
          link.parentElement.remove();
        } else {
          link.insertAdjacentElement("afterend", link.querySelector("img"));
          link.remove();
        }
      }
    }

    if (link.closest(".article-flag-list")) {
      return;
      // console.log("link forward to tags");
    }

    // if the link href = the content of the link
    if (
      !isLocalLink(link) &&
      (link.innerHTML.includes(link.getAttribute("href")) ||
        link.innerHTML?.startsWith("http"))
    ) {
      link.innerHTML = formatUrl(link.textContent);
      return;
      // console.log("the link seems to be included as text for: ", link);
    }

    // link that will have a full word
    if (!(isLocalLink(link) || link.href.includes("orcid.org/"))) {
      addWbrSpan(link);
    }
  });
}

function addWbrSpan(link) {
  link.insertAdjacentHTML(
    "beforeend",
    `<span class="prettified-symbol"> (</span><span class="prettified-link">${formatUrl(
      link.href
    )}</span><span class="prettified-symbol">) </span>`
  );
}

function link2spanWithWbr(content) {
  content.querySelectorAll("a").forEach((link) => {
    if (!link.href) return;

    if (isLocalLink(link) || link.href.includes("orcid.org/")) {
      return;
    }
    link.innerHTML = formatUrl(link.innerHTML);
  });
}

function hidenumber(content) {
  content.querySelectorAll("p").forEach((p) => {
    if (!p.innerHTML.match(/[A-z]/g)) {
      // p.classList.add("onlynumbers");
      p.remove();
    }
  });
}

function addClassToConsecutiveLinks(content) {
  content.querySelectorAll("span a + a").forEach((link) => {
    link.classList.add("nextlink");
  });
}

function removeMeta(content) {
  content.querySelectorAll("meta").forEach((el) => el.remove());
}

// To use to remove hyphens between pages
function getFinalWord(words) {
  var n = words.split(" ");
  return n[n.length - 1];
}

// add nbsp in list
function spaceToNbsp(content) {
  let authorList = content.querySelectorAll(
    ".authors li, .authors_authors-list__qIQOP li"
  );
  // let affiliationList = content.querySelector('.authors').nextElementSibling.querySelectorAll('li')

  const replaceSpace = (parent) => {
    if (parent.children.length)
      Array.from(parent.children).forEach((child) => replaceSpace(child));
    else parent.innerHTML = parent.innerHTML.replace(/\s+/g, "&nbsp;").trim();
  };
  authorList.forEach((person) => replaceSpace(person));
}

// add elife caution
function addElifeTextBeforeAbstract(content) {
  let abstract = content
    .querySelector("#evaluation-summary")
    .closest("section");
  abstract.insertAdjacentHTML(
    "beforebegin",
    `<section class="elife-caution">${
      content.querySelector(".evaluation-summary").innerHTML
    }</section>`
  );
  content.querySelector(".evaluation-summary").style.display = "none";
  // querySelector("#abstract")?.closest('section');
}
//move summary
function moveSummary(content) {
  let summaries = content.querySelectorAll(".evaluation-summary");
  content
    .querySelector("#abstract")
    .closest("section")
    .insertAdjacentElement("afterend", summaries[0]);
}
// rmeove scripts

function removescript(content) {
  let data = content.querySelector("#__next");
  if (!data) return;
  data.innerHTML = data.innerHTML.replace(/\<\!-+\s?\-+\>/g, "");
  content.querySelectorAll("script").forEach((script) => {
    script.innerHTML = "";
    script.remove();
  });
}

// create a table of content
let toctags = "h2, h3";

function createTOC(content) {
  let nav = document.createElement("nav");
  nav.innerHTML = "<h2>Table of contents</h2>";
  let toclist = document.createElement("ul");
  content.querySelectorAll(toctags).forEach((tag) => {
    toclist.insertAdjacentHTML(
      "beforeend",
      `<li class="toc-${tag.tagName.toLowerCase()}"><a href="#${
        tag.id
      }">${tag.innerHTML.trim()}</a></li>`
    );
  });
  nav.insertAdjacentElement("beforeend", toclist);
  content
    .querySelector(".evaluation-summary")
    .insertAdjacentElement("afterend", nav);
}

function splitCitationContentAndLink(content) {
  const links = content.querySelectorAll("a");
  links?.forEach((link) => {
    if (link.href.includes("#c")) {
      const hasParentSup = link.closest("sup");
      const linkText = link.textContent;

      if (hasParentSup || !isNaN(linkText)) return;
      const sup = document.createElement("sup");
      link.classList.add("updatedLink");
      //  const linkedElem = content.querySelector(`#${link.href.split('#')[1]}`)
      //  console.log(linkedElem);
      link.textContent = `${link.href.split("#c")[1]}`;

      const span = document.createElement("span");
      span.classList.add("linkText");
      span.textContent = linkText;
      if (link.classList.contains("nextElement"))
        span.classList.add("nextElement");
      sup.append(link.cloneNode(true));
      link.insertAdjacentElement("beforeBegin", span);
      link.replaceWith(sup);
    }
  });
}
//
/*========================== 
         addIDtoEachElement
    ========================== */

// Define here the tags you want to give id
let tags = [
  "figure",
  "figcaption",
  "img",
  "ol",
  "ul",
  "li",
  "p",
  "table",
  "h1",
  "h2",
  "h3",
  "h4",
  "div",
  "aside",
];

function addIDtoEachElement(content) {
  let total = 0;
  tags.forEach((tag) => {
    content.querySelectorAll(tag).forEach((el, index) => {
      if (!el.id) {
        if (el.tagName == "p") {
          if (el.closest("figcaption")) {
            return;
          }
        }

        el.id = `el-${el.tagName.toLowerCase()}-${index}`;
        total++;
      }
    });
  });
}

/*========================== 
         addLogoWithLink
    ========================== */

function addLogoWithLink(content) {
  const elem = `
    <a href="https://elifesciences.org/" class="linkElife">
      <img src="https://elifesciences.org/assets/patterns/img/patterns/organisms/elife-logo-xs.fd623d00.svg" alt='Elife' />
    </a> `;

  content.querySelector("div").insertAdjacentHTML("afterbegin", elem);
}
/*========================== 
         openAllDetailsAndSummary
    ========================== */

function openAllDetailsAndSummary(content) {
  content.querySelectorAll("details").forEach((detail) => {
    detail.setAttribute("open", "open");
  });
}

/*====================================== 
         Create the element used for runningHead 
    ====================================== */

function createElForRunningHead(content) {
  // get the author list for the running head
  //doi is coming from the document body
  let author = "";
  let authors = content.querySelectorAll(".authors li");
  if (authors.length == 1) {
    const link = authors[0].querySelector("a");
    author = `<a href=${link.href} class=${link.className}>${authors[0].innerHTML}</a>`;
  } else if (authors.length >= 2) {
    if (
      authors[0].children.length &&
      authors[0]?.children[0].nodeName !== "LINK"
    ) {
      const link = authors[0].querySelector("a");
      //get the surname. only work because they have a weird custom
      const authorName = link?.innerHTML?.trim().split("\x3C!--&nbsp;-->");
      author = `<a href=${link.href} class=${link.className}>${
        authorName[authorName.length - 1]
      }</a> <em>et al.</em>`;
    } else {
      const authorName = authors[0].innerHTML.split("&nbsp;");
      author = `${authorName[authorName.length - 1]} <em>et al.</em>`;
    }
  } else {
    for (let i = 0; i < authors.length; i++) {
      author = `${author}${
        i < authors.length - 1
          ? authors[i]?.innerHTML
          : ", " + authors[i]?.innerHTML
      }`;
    }
  }

  const fullYear = new Date(
    content.querySelector("dd time").getAttribute("datetime")
  ).getFullYear();

  // get the doi for the running header
  const doi = document.querySelector("body").dataset.doi;

  // export the running head
  content.querySelector("div").insertAdjacentHTML(
    "afterbegin",
    `<p class='runninghead'><span class="author">${author},</span>&nbsp;<span class"date">${fullYear} eLife.</span> <span class="doi">
      ${
        doi
          ? `&nbsp;<a href="https://doi.org/${doi}">https://doi.org/${doi}</a>`
          : ""
      }</span><span class="counter"></span></p>`
  );
}

function removeFigureList(content) {
  content.querySelector("#figures").closest("div").remove();
}

// get ratio for images and add classes based on taht.

// no hyphens between page
class noHyphenBetweenPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.hyphenToken;
  }

  afterPageLayout(pageFragment, page, breakToken) {
    if (pageFragment.querySelector(".pagedjs_hyphen")) {
      // find the hyphenated word
      let block = pageFragment.querySelector(".pagedjs_hyphen");

      // i dont know what that line was for :thinking: i removed it
      // block.dataset.ref = this.prevHyphen;

      // move the breakToken
      let offsetMove = getFinalWord(block.innerHTML).length;

      // move the token accordingly
      page.breakToken = page.endToken.offset - offsetMove;

      // remove the last word
      block.innerHTML = block.innerHTML.replace(
        getFinalWord(block.innerHTML),
        ""
      );

      breakToken.offset = page.endToken.offset - offsetMove;
    }
  }
}

Paged.registerHandlers(noHyphenBetweenPage);

class pushThings extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.pushblock = [];
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // move the element to the next bit
    if (declaration.property == "--experimental-push") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.pushblock.push([elId, declaration.value.value]);
      });
    }
  }
  afterParsed(parsed) {
    //reset the page break for the figures titles
    // let titles1 = parsed.querySelectorAll("h1,h2");
    // titles1.forEach((title, index) => {
    //   if (
    //     title.classList?.contains("probably-figures-title") &&
    //     !title.classList?.contains("skipBreakAfter")
    //   ) {
    //     title.dataset.breakAfter = "page";
    //   }
    //   if (
    //     title.classList?.contains("probably-figures-title-after") &&
    //     !title.classList?.contains("skipBreakBefore")
    //   ) {
    //     title.dataset.breakBefore = "page";
    //     delete title.dataset.breakAfter;
    //   }
    // });

    if (this.pushblock.length > 0) {
      this.pushblock.forEach((elToPush) => {
        const elem = parsed.querySelector(elToPush[0]);
        if (!elem) {
          return;
        }

        elem.dataset.pushBlock = elToPush[1];
        let direction = "";
        if (elToPush[1] < 0) {
          direction = "back";
        }
        if (direction == "back") {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.previousElementSibling) {
              elem.previousElementSibling.insertAdjacentElement(
                "beforebegin",
                elem
              );
            }
          }
        } else {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.nextElementSibling) {
              elem.nextElementSibling.insertAdjacentElement(
                "beforebegin",
                elem
              );
            }
          }
        }
      });
    }
  }
}

Paged.registerHandlers(pushThings);

class urlSwitcher extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    const imageUrl = document.body.id;
    content.querySelectorAll("img").forEach((img) => {
      img.src =
        "/images/" +
        imageUrl +
        "/" +
        img.src.split("/")[img.src.split("/").length - 1];
    });
    // find a place to put the content, i would say after the index
  }
}

// Paged.registerHandlers(urlSwitcher);

class CSStoClass extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatSameTop = [];
    this.floatSameBottom = [];
    this.floatNextTop = [];
    this.floatNextBottom = [];
    this.experimentalImageEdit = [];
    this.spacing = [];
    this.pushblock = [];
    this.experimentalMerged = [];
    this.fullPageBackground = [];
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // alter the image
    if (declaration.property == "--experimental-image-edit") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.experimentalImageEdit.push(elId);
      });
    }
    // move the element to the next bit
    else if (declaration.property == "--experimental-push") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.pushblock.push([elId, declaration.value.value]);
      });
    } else if (declaration.property == "--experimental-fullpage") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.fullPageBackground.push([elId, declaration.value.value]);
      });
    }
    //experimental merge
    else if (declaration.property == "--experimental-merge") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.experimentalMerged.push([elId, declaration.value.value]);
      });
    }
    // page floats
    else if (declaration.property == "--experimental-page-float") {
      if (declaration.value.value.includes("same-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameTop.push(sel.split(","));
        // console.log("floatSameTop: ", this.floatSameTop);
      } else if (declaration.value.value.includes("same-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameBottom.push(sel.split(","));
        //console.log("floatSameBottom: ", this.floatSameBottom);
      } else if (declaration.value.value.includes("next-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextTop.push(sel.split(","));
        //console.log('floatNextTop: ', this.floatNextTop);
      } else if (declaration.value.value.includes("next-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextBottom.push(sel.split(","));
        //console.log("floatNextBottom: ", this.floatNextBottom);
      }
    }
    // spacing
    else if (declaration.property == "--experimental-spacing") {
      var spacingValue = declaration.value.value;
      spacingValue = spacingValue.replace(/\s/g, "");
      spacingValue = parseInt(spacingValue);
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      var thisSpacing = [sel.split(","), spacingValue];
      this.spacing.push(thisSpacing);
    }
  }

  afterParsed(parsed) {
    if (this.pushblock.length > 0) {
      this.pushblock.forEach((elToPush) => {
        const elem = parsed.querySelector(elToPush[0]);
        if (!elem) {
          return;
        }

        elem.dataset.pushBlock = elToPush[1];
        let direction = "";
        if (elToPush[1] < 0) {
          direction = "back";
        }
        if (direction == "back") {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.previousElementSibling) {
              elem.previousElementSibling.insertAdjacentElement(
                "beforebegin",
                elem
              );
            }
          }
        } else {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.nextElementSibling) {
              elem.nextElementSibling.insertAdjacentElement("afterend", elem);
            }
          }
        }
      });
    }
    if (this.experimentalMerged.length > 0) {
      this.experimentalMerged.forEach((couple) => {
        const host = parsed.querySelector(couple[0]);
        const guest = parsed.querySelector(couple[1]);
        if (!host || !guest) {
          return;
        }
        guest.style.display = "none";
        host.classList.add("merged!");
        host.dataset.mergedGuest = guest.id;
        host.insertAdjacentHTML("beforeend", guest.innerHTML);
      });
    }
    if (this.experimentalImageEdit.length > 0) {
      this.experimentalImageEdit.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((img) => {
          img.classList.add("imageMover");

          // console.log("#" + img.id + ": image Mover");
        });
      });
    }
    if (this.floatNextBottom) {
      this.floatNextBottom.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-bottom");
          // console.log("#" + el.id + " moved to next-bottom");
        });
      });
    }
    if (this.floatNextTop) {
      this.floatNextTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-top");
          // console.log("#" + el.id + " moved to next-top");
        });
      });
    }
    if (this.floatSameTop) {
      this.floatSameTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-same-top");
          // console.log("#" + el.id + " moved to same-top");
        });
      });
    }
    if (this.floatSameBottom) {
      this.floatSameBottom.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-same-bottom");
          // console.log("#" + el.id + " moved to same-bottom");
        });
      });
    }
    if (this.spacing) {
      this.spacing.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist[0]).forEach((el) => {
          var spacingValue = elNBlist[1];
          var spacingClass = "spacing-" + spacingValue;
          // console.log(spacingClass);
          el.classList.add(spacingClass);
          // console.log("#" + el.id + " spaced " + spacingValue);
        });
      });
    }
    if (this.fullPageBackground) {
      this.fullPageBackground.forEach((background) => {
        parsed.querySelectorAll(background[0]).forEach((el) => {
          el.classList.add("moveToBackgroundImage");
        });
      });
    }
  }
}

Paged.registerHandlers(CSStoClass);

//float top

// lets you manualy add classes to some pages elements
// to simulate page floats.
// works only for elements that are not across two pages

let classElemFloatSameTop = "page-float-same-top"; // â† class of floated elements on same page
let classElemFloatSameBottom = "page-float-same-bottom"; // â† class of floated elements bottom on same page

let classElemFloatNextTop = "page-float-next-top"; // â† class of floated elements on next page
let classElemFloatNextBottom = "page-float-next-bottom"; // â† class of floated elements bottom on next page

class elemFloatTop extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.experimentalFloatNextTop = [];
    this.baseline = 22;
    this.experimentalFloatNextBottom = [];
    this.token;
  }

  layoutNode(node) {
    // If you find a float page element, move it in the array,
    if (node.nodeType == 1 && node.classList.contains(classElemFloatNextTop)) {
      let clone = node.cloneNode(true);
      this.experimentalFloatNextTop.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }
    if (
      node.nodeType == 1 &&
      node.classList.contains(classElemFloatNextBottom)
    ) {
      let clone = node.cloneNode(true);
      this.experimentalFloatNextBottom.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }

    if (
      node.nodeType == 1 &&
      node.classList.contains(classElemFloatSameBottom)
    ) {
      let clone = node.cloneNode(true);
      // this.experimentalFloatNextBottom.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }
  }

  beforePageLayout(page, content, breakToken) {
    //console.log(breakToken);
    // If there is an element in the floatPageEls array,
    if (this.experimentalFloatNextTop.length >= 1) {
      // Put the first element on the page.
      page.element
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement("afterbegin", this.experimentalFloatNextTop[0]);
      this.experimentalFloatNextTop.shift();
    }
    if (this.experimentalFloatNextBottom.length >= 1) {
      // Put the first element on the page.
      page.element
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement(
          "afterbegin",
          this.experimentalFloatNextBottom[0]
        );
      this.experimentalFloatNextBottom.shift();
    }
  }

  layoutNode(node) {
    if (node.nodeType == 1) {
      if (node.classList.contains(classElemFloatSameTop)) {
        let clone = node.cloneNode(true);
        clone.classList.add("figadded");
        document
          .querySelector(".pagedjs_pages")
          .lastElementChild.querySelector("article")
          .insertAdjacentElement("afterbegin", clone);
        node.style.display = "none";
        node.classList.add("hide");
      }

      if (
        node.previousElementSibling?.classList.contains(classElemFloatSameTop)
      ) {
        let img = document
          .querySelector(".pagedjs_pages")
          .lastElementChild.querySelector(`.${classElemFloatSameTop}`);
        // console.log(img)
        // console.log(node)
        // if (img?.clientHeight) {
        //   // count the number of line for the image
        //   let imgHeightLine = Math.floor(img.clientHeight / this.baseline)
        //   // add one light and get the height in pixeol
        //   img.dataset.lineOffset = imgHeightLine + 0
        //   img.style.height = `${(imgHeightLine + 0) * this.baseline}px`
        // }
      }
    }
  }
  // works only with non breaked elements
  afterPageLayout(page, content, breakToken) {
    // try fixed bottom on same if requested
    if (page.querySelector("." + classElemFloatSameBottom)) {
      var bloc = page.querySelector("." + classElemFloatSameBottom);
      bloc.classList.add("absolute-bottom");
      bloc.classList.add("figadded");
    }

    // try fixed bottom if requested
    if (page.querySelector("." + classElemFloatNextBottom)) {
      var bloc = page.querySelector("." + classElemFloatNextBottom);
      bloc.classList.add("absolute-bottom");
      bloc.classList.add("figadded");
    }
  }
}
Paged.registerHandlers(elemFloatTop);

/* url cut*/

class expMerge extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.experimentalMerged = [];
  }

  onDeclaration(declaration, dItem, dList, rule) {
    // alter the image
    //experimental merge
    if (declaration.property == "--experimental-merge") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.experimentalMerged.push([elId, declaration.value.value]);
      });
    }
  }

  beforeParsed(parsed) {
    if (this.experimentalMerged.length > 0) {
      this.experimentalMerged.forEach((couple) => {
        const host = parsed.querySelector(couple[0]);
        const guest = parsed.querySelector(couple[1]);
        if (!host || !guest) {
          return;
        }
        guest.style.display = "none";
        host.classList.add("merged!");
        host.dataset.mergedGuest = guest.id;
        host.insertAdjacentHTML("beforeend", guest.innerHTML);
      });
    }
  }
}

Paged.registerHandlers(expMerge);

class moveToParentFig extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.moveToParentFig = [];
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // move the element to the next bit
    if (declaration.property == "--experimental-moveToOutsideFigure") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.moveToParentFig.push([elId, declaration.value.value]);
      });
    }
  }
  beforeParsed(content) {
    if (this.moveToParentFig.length > 0) {
      this.moveToParentFig.forEach((elToPush) => {
        const elem = content.querySelector(elToPush[0]);
        if (!elem) {
          return;
        }
        let fighead = elem.querySelector("label").cloneNode(true);
        console.log("label", fighead);
        elem.insertAdjacentElement("beforeend", fighead);
        elem.closest("figure").insertAdjacentElement("afterend", elem);
      });
    }
  }
}
Paged.registerHandlers(moveToParentFig);

function formatUrl(url) {
  // Split the URL into an array to distinguish double slashes from single slashes
  var doubleSlash = url.split("//");

  // Format the strings on either side of double slashes separately
  var formatted = doubleSlash
    .map(
      (str) =>
        // Insert a word break opportunity after a colon
        str
          .replace(/(?<after>:)/giu, "$1<wbr>")
          // Before a single slash, tilde, period, comma, hyphen, underline, question mark, number sign, or percent symbol
          .replace(/(?<before>[/~.,\-_?#%])/giu, "<wbr>$1")
          // Before and after an equals sign or ampersand
          .replace(/(?<beforeAndAfter>[=&])/giu, "<wbr>$1<wbr>")
      // Reconnect the strings with word break opportunities after double slashes
    )
    .join("//<wbr>");

  return formatted;
}

function getFirstOf(element) {
  let firstElement = element;
  while (firstElement && firstElement.dataset.breakAfter == "avoid") {
    // console.log(firstElement);
    firstElement = firstElement.previousElementSibling;
  }

  return firstElement;
}

/** This is a rough draft */
async function formulaeTest(parsed) {
  const imagePromises = [];
  const paraImages = parsed.querySelectorAll("p img");
  paraImages.forEach((image) => {
    const img = new Image();
    let resolve, reject;
    const imageLoaded = new Promise(function (r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function () {
      const { widthClass, heightClass, ratioClass } = getSizeRatioClass(
        img.naturalWidth,
        img.naturalHeight
      );

      image.classList.add(widthClass, heightClass, ratioClass);

      const para = image.closest("p");

      for (child of para?.childNodes) {
        if (child.nodeType === Node.ELEMENT_NODE)
          para.classList.add(
            child.tagName === "IMG" ? "hasMultipleImages" : "hasOtherElem"
          );
        if (child.nodeType === Node.TEXT_NODE) para.classList.add("hasContent");
        if (para.matches(`p.hasMultipleImages.hasOtherElem.hasContent`)) break;
      }
      // console.log("loaded-------");
      para.classList.add("hasImage");
      resolve();
    };
    img.onerror = function () {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  try {
    return await Promise.all(imagePromises);
  } catch (err) {
    console.warn(err);
  }
}

function getSizeRatioClass(width, height) {
  return {
    widthClass: getSizeClass(width, "width"),
    heightClass: getSizeClass(height, "height"),
    ratioClass: getRatioClass(width / height),
  };
}
function getSizeClass(size, paramStr) {
  if (size <= 48) return `${paramStr}-40`;
  if (size <= 80) return `${paramStr}-80`;
  if (size <= 160) return `${paramStr}-160`;
  if (size <= 240) return `${paramStr}-240`;
  if (size <= 360) return `${paramStr}-360`;
  if (size <= 480) return `${paramStr}-480`;
  return `${paramStr}-480-above`;
}

function getRatioClass(ratio) {
  if (ratio >= 1.7) return "landscape";
  if (ratio <= 0.95) return "portrait";
  if (ratio < 1.39 || ratio > 0.95) return "square";
}

function remixContent(content) {
  //remove figure containers
  content.querySelectorAll(".figure-container").forEach((el) => {
    el.insertAdjacentHTML("beforeBegin", el.innerHTML);
    el.remove();
  });

  // this is the content we’re going forward to
  const header = content.querySelector(".content-header");

  // get the copyright part
  let copyrights = `<ul class="copyrights">`;
  Array.from(content.querySelectorAll(".descriptors__icon")).map((info) => {
    copyrights += `<li class="${info.classList}"> <a class="donttouch" href="${info.href}"><span class="infourl">${info.href}</span><span class="hidden">${info.textContent}</span></a></li>`;
  });
  copyrights += "</li>";

  // simpler things
  const sideSection = content.querySelector(".side-section");
  const assesment = content.querySelector("#assessment");
  const abstract = content.querySelector(".abstract");
  const article = content.querySelector(".article-body");
  const references = content.querySelector("#references")?.closest("section");
  const authors = content.querySelector(".author-list");
  const logo = content.querySelector(".site-header-container img");
  const authorresponse = content.querySelector("#author-response");
  const logoBaseline = content.querySelector(
    ".site-header-container .site-header__title"
  );

  //get the reviewers
  const reviewers = content.querySelector(".editors-and-reviewers");

  // get the reviews part
  let reviews = getReviewsContent(content);

  // citation is not available but it would be nice to have
  // const citation = content.querySelector(".citation")

  // things to remove (instead of using css)
  let toRemove = [
    ".authors-list__expansion",
    ".institutions-list__expansion",
    "button",
    ".article-actions",
    "header .descriptors__identifiers",
    "header .descriptors",
    ".modal-container",
  ];

  //keep previous
  // document.querySelectorAll(".descriptors__identifiers a").forEach(el=>{
  //   console.log(el.outerHTML)
  // })

  // to remove
  toRemove.forEach((torem) => {
    content.querySelectorAll(torem).forEach((element) => {
      element.remove();
    });
  });

  content.querySelector("div").insertAdjacentHTML(
    "afterend",
    `<article>
  <section id="logo"> ${
    logo.outerHTML
  } <p class="baseline" style="display: none">${
      logoBaseline.innerHTML
    }</p> </section>
    <section id="sideSection">${sideSection.innerHTML} </section>
  <header id="header"> ${header ? header.innerHTML : ""} ${
      copyrights ? copyrights : ""
    }</header>

 ${assesment ? `<section id="assesment">${assesment.innerHTML}</section>` : ""}
  <section id="abstract">${abstract.innerHTML}</section>

  <section id="article-content">${article.innerHTML}</section>
  ${
    references
      ? `<section id="references-wrapper">${references.innerHTML}</section>`
      : ""
  }
  ${
    authors
      ? `<section id="authors"><h2>Author information</h2> <ul> ${authors?.innerHTML} </ul> </section>`
      : ""
  }
  ${
    reviewers?.innerHTML
      ? `<section id="reviewers">${reviewers?.innerHTML}</section>`
      : ""
  }
  ${reviews ? `<section id="reviews"> ${reviews}</section>` : ""}
  ${
    authorresponse
      ? `<section id="authorresponse">${authorresponse?.innerHTML}</section>`
      : ""
  }
</article>`
  );

  content.querySelector("div").remove();
}

function isLocalLink(link) {
  // Create an anchor element
  const anchor = document.createElement("a");
  anchor.href = link;

  // Get the hostnames of the link and current page
  const linkHostname = anchor.hostname;
  const currentHostname = window.location.hostname;

  // Compare the hostnames
  return linkHostname === currentHostname;
}

/*========================== 
         addLogoWithLink
    ========================== */

function addLogoWithLink(content) {
  const elem = `
    <a class="logo elife-intro linkElife" href="https://elifesciences.org/">
      <img src="https://elifesciences.org/assets/patterns/img/patterns/organisms/elife-logo-xs.fd623d00.svg" alt='Elife' />
    </a> `;

  content.querySelector("article").insertAdjacentHTML("afterbegin", elem);
}

/*========================== 
        chek if link is made only of an image 
    ========================== */

function isLinkContainingOnlyImage(link) {
  // Get the link element

  // Check if the link element exists
  if (link) {
    // Get all the child nodes of the link element
    var childNodes = link.childNodes;

    // Iterate over the child nodes
    for (var i = 0; i < childNodes.length; i++) {
      var node = childNodes[i];

      // Check if the child node is an image element
      if (node.tagName === "IMG") {
        // If an image element is found, continue checking other child nodes
        continue;
      }

      // If any child node is found that is not an image element,
      // return false indicating that the link contains more than just an image
      return false;
    }

    // If no child node is found that is not an image element,
    // return true indicating that the link contains only an image
    return true;
  }

  // If the link element does not exist, return false
  return false;
}

// merge blockquotes to have only one when needed.
function mergeBlockquotes(content) {
  const blockquotes = content.querySelectorAll("blockquote");

  blockquotes.forEach((blockquote) => {
    if (
      blockquote.previousElementSibling &&
      blockquote.previousElementSibling.tagName === "BLOCKQUOTE"
    ) {
      blockquote.innerHTML =
        blockquote.previousElementSibling.innerHTML + blockquote.innerHTML;
      blockquote.previousElementSibling.parentNode.removeChild(
        blockquote.previousElementSibling
      );
    }
  });
}

// check if the element is the only child
//
//
//

function isElementTheOnlyChild(el) {
  // Get the parent element of the span
  var parentElement = el.parentNode;

  // Check if the parent has only one child node and it is the el
  if (
    parentElement.childNodes.length === 1 &&
    parentElement.firstChild === el
  ) {
    return true;
  }

  // Check if all other child nodes are text nodes or empty nodes
  for (var i = 0; i < parentElement.childNodes.length; i++) {
    var childNode = parentElement.childNodes[i];

    // Ignore the el itself
    if (childNode === el) {
      continue;
    }

    // Check if the child node is a text node or empty node
    if (childNode.nodeType === Node.TEXT_NODE) {
      var textContent = childNode.textContent.trim();

      // If the text content is not empty, return false
      if (textContent !== "") {
        return false;
      }
    } else {
      // If the child node is not a text node, return false
      return false;
    }
  }

  // If all conditions are met, return true
  return true;
}

// function to fix when the last child of the last element of the page has a break after avoid

// check p
//
//
//

// let’s remove the margin top when the element is the first on the page
//
//o
//
//

class fixMarginTop extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.pagedone = false;
    let hasRenderedContent = false;
  }

  onPageLayout() {
    this.hasRenderedContent = false;
    this.pagedone = false;
  }

  renderNode(node) {
    if (this.pagedone == true) return;
    if (node.nodeType != 1) return;

    if (!this.hasRenderedContent) {
      this.hasRenderedContent = hasContent(node);
    }

    if (this.hasRenderedContent == true) {
      // check if the starting page is really at 0
      const wrapper = node
        .closest(".pagedjs_page_content")
        .querySelector("div");
      let wrapperOffsetTop = wrapper.offsetTop;
      if (wrapperOffsetTop != 0) {
        // console.log(wrapperOffsetTop);
        // console.log(wrapper.style.marginTop);
        // wrapper.style.position = `relative`
        // wrapper.style.marginTop = `-${wrapperOffsetTop}px`;
      }

      // console.log(wrapperOffsetTop);
    }
    this.pagedone = true;
  }

  // check if node is the first element
}

function isElement(node) {
  return node && node.nodeType === 1;
}

function isText(node) {
  return node && node.nodeType === 3;
}

function hasContent(node) {
  if (isElement(node)) {
    return true;
  } else if (isText(node) && node.textContent.trim().length) {
    return true;
  }
  return false;
}

Paged.registerHandlers(fixMarginTop);

function dispatchFigure(content) {
  content.querySelectorAll("figure").forEach((fig) => {
    // if there is no label keep the image together
    if (fig.classList.contains("no-label")) {
      return;
    }

    if (fig.querySelectorAll("img").length > 1) {
      // not to fill is not yet added.
      // we need to do that before anythin
      if (fig.classList.contains("nottofill")) {
        return;
      }

      // get the last figure
      let imageToMove = [...fig.querySelectorAll("img")];

      // Filter out images that are children of <figcaption> elements
      imageToMove = imageToMove.filter((img) => !img.closest("figcaption"));

      // create figures for sub images block
      for (let index = imageToMove.length; index > 1; index--) {
        const element = imageToMove[index - 1];

        // link figure
        fig.classList.add("has-continued");
        fig.dataset.continuedMain = `ctn-${fig.id}`;
        fig.insertAdjacentHTML(
          "afterend",
          `<figure data-continued-from="ctn-${
            fig.id
          }" class="added-figure-tofill tofill continuedContent" id="${
            fig.id
          }-cont-${index}"><figcaption><p data-brea-after="unset" class="continued"><span class="label figure-name">${
            fig?.querySelector(".label")
              ? fig.querySelector(".label").innerHTML
              : ""
          }</span> <span class="ctn">(continued)</span></p></figcaption>${
            element.outerHTML
          }</figure>`
        );
        element.remove();
      }
    }
  });
}

// if figure have multiple images, just put the image into following figures
// so <fig > img1 + img2
// becomes
// <fig > img1 > + fig img2 etc.
// add a class to those figure so they’re following the main process instead of being rendered as image outisde of figures
//
//

// remove the second title of the abstract if it’s summary or abstract.
function fixAbstract(content) {
  let titles = [...content.querySelector("#abstract").querySelectorAll("h1")];
  if (titles.length > 1) {
    titles.shift();
    titles.forEach((title) => {
      // if (title.textContent == "Summary" || title.textContent == "Abstract") {
      //   title.remove();
      // }
    });
  }
}

async function addWidthHeightToImg(parsed) {
  let imagePromises = [];
  let images = parsed.querySelectorAll("img");
  images.forEach((image) => {
    if (image.width && image.height) return;
    let img = new Image();
    let resolve, reject;
    let imageLoaded = new Promise(function (r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function () {
      let height = img.naturalHeight;
      let width = img.naturalWidth;
      image.setAttribute("height", height);
      image.setAttribute("width", width);

      let ratio = width / height;
      if (ratio >= 1.7) {
        image.classList.add("landscape");
        image.parentNode.classList.add("fig-landscape");
      } else if (ratio <= 0.95) {
        image.classList.add("portrait");
        image.parentNode.classList.add("fig-portrait");
      } else if (ratio < 1.39 || ratio > 0.95) {
        image.classList.add("square");
        image.parentNode.classList.add("fig-square");
      }
      resolve();
    };
    img.onerror = function () {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  return Promise.all(imagePromises).catch((err) => {
    console.warn("err", err);
  });
}

//reorder figure
function reorderfigures(content) {
  content.querySelectorAll("label").forEach((label) => {
    if (!label.closest("figure")) return;
    const fig = label.closest("figure");
    const figcaption = fig.querySelector("figcaption");
    let text = fig.querySelector("figcaption");

    // if there is no figcaption createone
    if (!text) {
      fig.insertAdjacentHTML(
        "afterbegin",
        `<ficaption><h3><span class= "label figure-name">${label.innerHTML}</span ></h3></figcaption>`
      );
    } else if (text?.querySelector("h3")) {
      text
        .querySelector("h3")
        ?.insertAdjacentHTML(
          "afterbegin",
          `<span class="label figure-name">${label.innerHTML}</span>`
        );
    } else if (text) {
      text.insertAdjacentHTML(
        "afterbegin",
        `<h3><span class="label figure-name">${label.innerHTML}</span></h3>`
      );
    }

    label?.remove();
    figcaption?.remove();

    // the new caption
    let newCaption = `<figcaption>${text ? text.innerHTML : ""}</figcaption>`;
    fig.insertAdjacentHTML("afterbegin", newCaption);
  });
}

function addCorrespondingAuthor(content) {
  // change the author email system:
  content
    .querySelectorAll(".authors-email__link .visuallyhidden")
    .forEach((el) => {
      el.classList.add("email-icon");
      el.innerHTML = "";
    });
}

// get the content for the reviews
function getReviewsContent(content) {
  let reviews = "";

  content.querySelectorAll(".review-content").forEach((review) => {
    if (review.id.includes("peer-review")) {
      // console.log(review);
      reviews += review.outerHTML;
    }
  });
  // console.log("reviews", reviews);
  return reviews;
}

//make all the links clickable by adding a small icon before. let’s go
//
function makeAllLinksClickable(content) {
  // return
  let className = "";

  content.querySelectorAll("#article-content a").forEach((link) => {
    //if link -> reference
    if (link.href.includes("#c")) {
      className = "linktoref";
    }
    //if link -> figures
    else if (link.href.includes("#fig") || link.href.includes("#tbl")) {
      className = "linktofig";
    }
    //if link -> figures
    else if (!isLocalLink(link.href)) {
      className = "linktoext";
    }

    let newUrl = `<span class="fakelink-text">${link.innerHTML}</span><a class="fake-icon" href="${link.href}">${linkicon}</a>`;

    link.insertAdjacentHTML(
      "afterend",
      `<span class="fakelink ${className}">${newUrl}</span>`
    );
    link.remove();
  });
}

let linkicon = `<span class="fakelink-icon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>`;
// let linkicon = `<svg width="10" height="10" style="display:"contents", line-height: 0;" fill="color: var(--color-primary")" class="svg-icon" viewBox="0 0 20 20"><path d="M16.198,10.896c-0.252,0-0.455,0.203-0.455,0.455v2.396c0,0.626-0.511,1.137-1.138,1.137H5.117c-0.627,0-1.138-0.511-1.138-1.137V7.852c0-0.626,0.511-1.137,1.138-1.137h5.315c0.252,0,0.456-0.203,0.456-0.455c0-0.251-0.204-0.455-0.456-0.455H5.117c-1.129,0-2.049,0.918-2.049,2.047v5.894c0,1.129,0.92,2.048,2.049,2.048h9.488c1.129,0,2.048-0.919,2.048-2.048v-2.396C16.653,11.099,16.45,10.896,16.198,10.896z"></path><path d="M14.053,4.279c-0.207-0.135-0.492-0.079-0.63,0.133c-0.137,0.211-0.077,0.493,0.134,0.63l1.65,1.073c-4.115,0.62-5.705,4.891-5.774,5.082c-0.084,0.236,0.038,0.495,0.274,0.581c0.052,0.019,0.103,0.027,0.154,0.027c0.186,0,0.361-0.115,0.429-0.301c0.014-0.042,1.538-4.023,5.238-4.482l-1.172,1.799c-0.137,0.21-0.077,0.492,0.134,0.629c0.076,0.05,0.163,0.074,0.248,0.074c0.148,0,0.294-0.073,0.382-0.207l1.738-2.671c0.066-0.101,0.09-0.224,0.064-0.343c-0.025-0.118-0.096-0.221-0.197-0.287L14.053,4.279z"></path></svg>`;

/*break after*/
// check if the element has a break after avoid and move it on next page
class avoidBreakAfter extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  finalizePage(pageFragment, page) {
    // remove any final emtpy element

    pageFragment.querySelectorAll("ol").forEach((ol) => {
      // remove any empty ol at the end of any page: if ol is the last element and is empty
      if (ol.textContent.length < 1 && ol.parentElement.lastChild == ol) {
        ol.remove();
      }
    });

    let possibleBreakAvoid = pageFragment.querySelectorAll(
      `[data-break-after="avoid"]`
    );

    let goodEl;

    possibleBreakAvoid.forEach((el) => {
      // if element hs been marked as having nothing after to stick to.

      if (el.classList.contains("no-el-next")) return;
      if (el.classList.contains("done")) return;
      // if (el.nextElementSibling) return;
      if (el.closest(".movedfig")) return;
      if (el.closest("figure")) return;
      if (el.parentElement.tagName == "FIGURE") {
        // .... there shouldnt be any figures anymore
      }
      // el.classList.add("touched")
      //

      if (
        !el.nextElementSibling &&
        !el.parentElement.nextElementSibling &&
        !el.tagName == "SECTION"
      ) {
        goodEl = el;

        // console.log(el.nextElementSibling);

        while (
          goodEl.previousElementSibling &&
          goodEl.previousElementSibling.dataset.breakAfter == "avoid"
        ) {
          goodEl.classList.add("toremove");
          goodEl = goodEl.previousElementSibling;
          debugger;
        }
      }
    });

    //if there is no element at the end keep going
    if (!goodEl) return;

    console.log("goodel", goodEl.dataset.ref);

    // console.log(page);
    const elementFromSource = this.chunker.source.querySelector(
      `[data-ref="${goodEl.dataset.ref}"]`
    );

    elementFromSource.dataset.breakAfter = "none";

    // elementFromSource.data

    while (goodEl.nextElementSibling) {
      goodEl.nextElementSibling.remove();
    }
    // goodEl.remove();

    // switch the breaktoken
    if (page.endToken) {
      page.breakToken = page.endToken.node = elementFromSource;
      page.breakToken = page.endToken.offset = 0;
    }
  }
}
Paged.registerHandlers(avoidBreakAfter);

function getDeepestBlockLastChild(parentElement) {
  let lastChild = parentElement.lastElementChild;

  const smallerTags = ["SVG", "P", "LI", "IMG"];

  // until you get the nested last child
  while (lastChild.lastElementChild) {
    if (smallerTags.includes(lastChild.tagName)) {
      return lastChild;
    }
    lastChild = lastChild.lastElementChild;
  }

  if (!lastChild) {
    return null;
  }

  return lastChild;
}

// if (deepestBlockLastChild) {
//   console.log(deepestBlockLastChild);
// } else {
//   console.log("No deepest block-level last child found.");
// }

function markImages(content) {
  content.querySelectorAll("img").forEach((img) => {
    // Check if the element has a parent
    img.classList.add(`child-of-${img.parentElement?.tagName.toLowerCase()}`);

    if (img.parentElement.tagName == "P") {
      // Get the parent of the element
      const parent = img.parentElement;

      // Check if the element is the only child of its parent
      if (parent.childElementCount == 1 && parent.textContent.length < 33) {
        parent.classList.add("imageonly");
      } else {
        img.classList.add("insidetext");
      }
    }
  });
}

function removePictures(content) {
  let pictures = content.querySelectorAll("picture");
  pictures.forEach((picture) => {
    picture.insertAdjacentHTML(
      "beforebegin",
      picture.querySelector("img").outerHTML
    );
    picture.remove();
  });
}

function getHighestResImg(element) {
  if (element.getAttribute("srcset")) {
    return element
      .getAttribute("srcset")
      .split(",")
      .reduce(
        (acc, item) => {
          let [url, width] = item.trim().split(" ");
          width = parseInt(width);
          if (width > acc.width) return { width, url };
          return acc;
        },
        { width: 0, url: "" }
      ).url;
  }

  return element.getAttribute("src");
}

function truncateRefernceAuthors(content) {
  const MAX_AUTHORS = 10;

  const allReferences = content?.querySelectorAll(
    ".reference-list  .reference .reference__authors_list"
  );

  allReferences.forEach((reference) => {
    const referenceAuthors = reference.querySelectorAll(".reference__author");

    if (referenceAuthors?.length > MAX_AUTHORS) {
      const firstChildClone = referenceAuthors[0].cloneNode(true);
      firstChildClone.innerHTML += " <em>et al.</em> ";
      reference.innerHTML = "";
      reference.insertAdjacentElement("beforeend", firstChildClone);
    }
  });
}

// guess the reference supplementary figuresXCV
function guessSupp(content, index) {
  const sections = content.querySelectorAll("article section");

  sections.forEach((section) => {
    let sectionScore = 0;

    // check the id of the section
    if (section.id.includes("figure")) {
      sectionScore += 100;
    }

    // if section has a title that includes Supplementay figures
    let title = section.querySelector("h1");

    if (
      title?.textContent.includes("Supplementary") ||
      title?.textContent.includes("supplementary") ||
      title?.textContent.includes("figures") ||
      title?.textContent.includes("Figures")
    ) {
      sectionScore += 100;
    }

    // if section has figures only or figures or more than 90% of the content figures, it score 100. (+ 20 pt if figure id ends with s\d)
    // console.log("children", section.children.length)
    let children = section.children;
    let childrenFigure = section.querySelectorAll("figure");
    if (childrenFigure.length / children.length > 0.9) {
      sectionScore = sectionScore + 300;
    }

    // checking figures
    section.querySelectorAll("figure").forEach((fig) => {
      // if figure id contains s1 or s2 (typically for supplementary figures)
      const pattern = /s\d+$/;
      if (pattern.test(fig.id)) {
        sectionScore = sectionScore + 20;
      }
    });

    section.dataset.suppSection = sectionScore;

    if (
      section.dataset.suppSection >
      section?.previousElementSibling?.dataset.suppSection
    ) {
      section.previousElementSibling.classList.remove("surelySupp");
      section.classList.add("surelySupp");
    }
  });
}

function fixCClink(content) {
  const ccUrl = content.querySelector(".descriptors__icon--cc");

  if (isLocalLink(ccUrl.href)) {
    ccUrl.classList.add("insideurl");
  }
}

function tagFigures(content) {
  // check will page will be set as full page
  content.querySelectorAll("figure").forEach((fig) => {
    if (!fig.querySelector("label")) {
      const p = document.createElement("p");

      // make a paragraph with the image if it has no label because it means it doesnt need to be set differently
      p.innerHTML = fig.innerHTML;
      p.id = fig.id;
      p.classList = fig.classList;
      p.classList.add("movedfig");
      fig.insertAdjacentElement("beforebegin", p);

      // remove the figure
      fig.remove();
      return;
    }

    // add the element to fill to the figures
    fig.classList.add("tofill");
  });
}

function pStrongToTitles(content) {
  content.querySelectorAll("strong").forEach((el) => {
    if (el.textContent.length == el.closest("p")?.textContent.length) {
      if (isElementTheOnlyChild(el)) {
        if (el == el.parentElement.firstChild && !el.closest("li")) {
          el.parentElement.classList.add("probablyHeading");
        }
      }
    }
  });
}

function recreateRunningHead() {
  //only for elife, check the runningheadre
  // add the running head
  let runninghead = document.querySelector(".runninghead");
  document.querySelectorAll(".pagedjs_pagedjs-filler_page").forEach((page) => {
    // this is for elife only, fix the runningHead
    if (runninghead) {
      page
        .querySelector(".pagedjs_pagebox")
        .insertAdjacentElement("afterbegin", runninghead.cloneNode(true));
    }
  });
}

function tagImgInFigures(content) {
  const figures = content.querySelectorAll("figure");
  figures.forEach((figure) => {
    //number of images
    figure.classList.add(`imgs-${figure.querySelectorAll("img").length}`);
    //
    // is there a title
    // TODO
    // figure.classList.add(`has-title`);
    // is there a caption

    if (
      !figure.querySelector("figcaption p") &&
      !figure.querySelector("label")
    ) {
      figure.classList.add("font");
      figure.classList.add("no-caption");
    }
    // how long is the caption
    // when the caption is less than 550, makes it one column

    let captionLength = 0;
    figure
      .querySelector("figcaption")
      ?.querySelectorAll("p, li")
      .forEach((el) => {
        captionLength = captionLength + el.textContent.length;
      });

    // console.log(figure.id, captionLength);
    if (captionLength < 800 && captionLength >= 500) {
      figure.classList.add("shortcaption");
    } else if (captionLength < 500) {
      figure.classList.add("shortercaption");
    } else {
      figure.classList.add("longcaption");
    }

    // if it’s a continued element, don’t show its name
    if (figure.querySelector(".ctn")) return;

    // figure.insertAdjacentHTML(
    //   "afterend",
    //   `<p class="sendtolink">See ${figure
    //     .querySelector("h3").innerHTML}</p>`
    // );
  });
}
